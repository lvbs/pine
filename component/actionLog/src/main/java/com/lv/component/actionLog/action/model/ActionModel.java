package com.lv.component.actionLog.action.model;

import lombok.Getter;

/**
 * @author 黄山松
 * @date 2018/10/15
 */
@Getter
public class ActionModel {
    /** 日志名称 */
    protected String name;

    /** 日志类型 */
    protected Byte type;
}
