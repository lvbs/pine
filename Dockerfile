FROM openjdk:8
VOLUME /usr/tmp
COPY admin/target/Pine-2.0.3.jar Pine-2.0.3.jar
RUN bash -c "touch /Pine-2.0.3.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","Pine-2.0.3.jar"]

